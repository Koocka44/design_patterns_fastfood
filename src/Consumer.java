public class Consumer {

	private double happiness;

	public double getHappiness() {
		return happiness;
	}

	public void setHappiness(double newHappiness) {
		this.happiness = newHappiness;
	}

	public void consume(Food food) {
		food.getConsumed(this);
	}
}
