public class Ketchup implements Extra {

	@Override
	public void apply(Food food) {
		food.setAddHappiness(food.getAddHappiness() * 2);
		food.setIncreaseHappinessPercent(food.getIncreaseHappinessPercent() * 2);
	}

}
