import java.util.ArrayList;
import java.util.List;

public class OrderBuilder {

	private List<Extra> extras;
	private Food food;

	public OrderBuilder() {
		this.extras = new ArrayList<Extra>();
	}

	public Order createOrder() {
		return new Order(food, (Extra[]) extras.toArray());
	}
	
	public OrderBuilder setFood(Food food){
		this.food = food;
		return this;
	}
	
	public OrderBuilder addExtra(Extra extra){
		extras.add(extra);
		return this;
	}
}
