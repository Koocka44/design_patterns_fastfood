import java.util.List;

public class Robot implements Observer {

	private OrderFifo fifo;
	private Consumer consumer;
	
	public Robot(Consumer consumer) {
		this.fifo = OrderFifo.getInstance();
		this.fifo.attach(this);
		this.consumer = consumer;
	}
	
	@Override
	public void update() {
		if(fifo.hasMoreOrders()){
			Food food = processOrder(fifo.getNextFromQueue());
			consumer.consume(food);
		}
	}

	private Food processOrder(Order order) {
		Food food = order.getFood();
		List<Extra> extras = order.getExtras();
		for(Extra extra : extras){
			extra.apply(food);
		}
		return food;
	}

}
