public abstract class Food {

	private int addHappiness;
	private int increaseHappinessPercent;

	public Food(int addHappiness, int increaseHappinessPercent) {
		this.addHappiness = addHappiness;
		this.increaseHappinessPercent = increaseHappinessPercent;
	}

	public void getConsumed(Consumer consumer) {
		double happiness = consumer.getHappiness();
		double newHappiness = (happiness + addHappiness)
				* (1 + increaseHappinessPercent / 100.0);
		consumer.setHappiness(newHappiness);
	}

	public int getAddHappiness() {
		return addHappiness;
	}

	public void setAddHappiness(int addHappiness) {
		this.addHappiness = addHappiness;
	}

	public int getIncreaseHappinessPercent() {
		return increaseHappinessPercent;
	}

	public void setIncreaseHappinessPercent(int increaseHappinessPercent) {
		this.increaseHappinessPercent = increaseHappinessPercent;
	}

}
