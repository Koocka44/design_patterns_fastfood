public class FoodFactory {

	private Ketchup ketchup = new Ketchup();
	private Mustard mustard = new Mustard();

	public Food createHotDogWithKetchup() {
		HotDog hotdog = new HotDog();
		ketchup.apply(hotdog);
		return hotdog;
	}

	public Food createHotDogWithMustard() {
		HotDog hotdog = new HotDog();
		mustard.apply(hotdog);
		return hotdog;
	}

	public Food createChipsWithKetchup() {
		Chips chips = new Chips();
		ketchup.apply(chips);
		return chips;
	}

	public Food createChipsWithMustard() {
		Chips chips = new Chips();
		mustard.apply(chips);
		return chips;
	}

}
