import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;


public class OrderFifo implements Observable{

	private static OrderFifo instance;
	private LinkedList<Order> fifo = new LinkedList<Order>();
	private Set<Observer> observers = new HashSet<Observer>();
	
	private OrderFifo(){
		
	}
	
	public synchronized static OrderFifo getInstance(){
		if(instance == null){
			instance = new OrderFifo();
		}
		return instance;
	}
	
	public synchronized void addToQueue(Order order){
		this.fifo.add(order);
	}
	
	public synchronized boolean hasMoreOrders(){
		return !this.fifo.isEmpty();
	}
	
	public synchronized Order getNextFromQueue(){
		Order order = null;
		if(!this.fifo.isEmpty()){
			order = fifo.removeFirst();
		}
		return order;
	}
	
	public synchronized void attach(Observer observer){
		this.observers.add(observer);
	}
	
	public void notifyAllObservers(){
		for(Observer observer : observers){
			observer.update();
		}
	}
}
