import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Order {

	private Food food;
	private List<Extra> extras;

	public Order(Food food, Extra... extras) {
		this.food = food;
		this.extras = new ArrayList<Extra>();
		this.extras.addAll(Arrays.asList(extras));
	}

	public Food getFood() {
		return food;
	}

	public List<Extra> getExtras() {
		return extras;
	}

}
