public interface Extra {

	public void apply(Food food);
}
