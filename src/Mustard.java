public class Mustard implements Extra {

	@Override
	public void apply(Food food) {
		food.setAddHappiness(1);
		food.setIncreaseHappinessPercent(0);
	}

}
